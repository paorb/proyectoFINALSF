package ejemplos.navigation2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import ejemplos.ws.HttpManager;


public class Login extends Activity {

    HttpManager manager;
    Button login;

    EditText user;
    EditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user = (EditText)findViewById(R.id.user);
        pass = (EditText)findViewById(R.id.pass);

        login = (Button)findViewById(R.id.login);
        login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                autenticar autenticar = new autenticar();
                autenticar.execute(user.getText().toString(), pass.getText().toString());

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class autenticar extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {

            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {
                obj.put("username", params[0]);
                obj.put("password", params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/autenticar", "POST", obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals(true)){
                Intent tomain = new Intent(Login.this, MainActivity.class);
                startActivity(tomain);

                Toast.makeText(Login.this, "Identificacion Exitosa", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
