package ejemplos.navigation2;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import ejemplos.ws.HttpManager;

public class RegAct_Cancha extends Fragment {

    HttpManager manager;
    Button regcan, actcan;
    EditText codigo, nombre, carac;


    public RegAct_Cancha() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.regact_cancha, container, false);

        regcan = (Button) rootview.findViewById(R.id.reg_can);
        actcan = (Button) rootview.findViewById(R.id.act_can);
        codigo = (EditText) rootview.findViewById(R.id.cod_reg_can);
        nombre = (EditText) rootview.findViewById(R.id.nom_reg_can);
        carac = (EditText) rootview.findViewById(R.id.car_reg_can);

        regcan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrarCancha reg = new RegistrarCancha();
                reg.execute(nombre.getText().toString(), carac.getText().toString());

            }
        });

        actcan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActualizarCancha act = new ActualizarCancha();
                act.execute(codigo.getText().toString(), nombre.getText().toString(), carac.getText().toString());
            }
        });

        return rootview;
    }

    private class RegistrarCancha extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {
                obj.put("nombre_cancha", params[0]);
                obj.put("caracteristicas", params[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/create", "POST", obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("true")) {
                Toast.makeText(RegAct_Cancha.this.getActivity(), "Cancha Registrada", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class ActualizarCancha extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {
                obj.put("codigo_cancha", params[0]);
                obj.put("nombre_cancha", params[1]);
                obj.put("caracteristicas", params[2]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/edit", "PUT", obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("true")) {
                Toast.makeText(RegAct_Cancha.this.getActivity(), "Cancha Actualizada", Toast.LENGTH_SHORT).show();
            }
        }
    }
}