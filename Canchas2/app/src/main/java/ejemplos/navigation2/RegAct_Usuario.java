package ejemplos.navigation2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import ejemplos.ws.HttpManager;

public class RegAct_Usuario extends Fragment {

    HttpManager manager;
    Button regusu, actusu;
    EditText codigo, usuario, contrasenia;


    public RegAct_Usuario(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.regact_usuario, container, false);

        regusu = (Button)rootview.findViewById(R.id.reg_usu);
        actusu = (Button)rootview.findViewById(R.id.act_usu);
        codigo = (EditText)rootview.findViewById(R.id.cod_reg_usu);
        usuario = (EditText)rootview.findViewById(R.id.usu_reg_usu);
        contrasenia = (EditText)rootview.findViewById(R.id.con_reg_usu);

        regusu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrar reg = new Registrar();
                reg.execute(usuario.getText().toString(), contrasenia.getText().toString());

            }
        });

        actusu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actualizar act = new Actualizar();
                act.execute(codigo.getText().toString(), usuario.getText().toString(), contrasenia.getText().toString());
            }
        });

        return rootview;
    }

    private class Registrar extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {
                obj.put("username", params[0]);
                obj.put("password", params[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/create3","POST",obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("true")){
                Toast.makeText(RegAct_Usuario.this.getActivity(), "Usuario Registrado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class Actualizar extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {
                obj.put("codigo_usuario", params[0]);
                obj.put("username", params[1]);
                obj.put("password", params[2]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/edit3","PUT",obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("true")){
                Toast.makeText(RegAct_Usuario.this.getActivity(), "Usuario Actualizado", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
