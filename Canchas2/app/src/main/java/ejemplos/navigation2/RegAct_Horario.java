package ejemplos.navigation2;

import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ejemplos.dialogs.Dialog_Hora;
import ejemplos.ws.HttpManager;

public class RegAct_Horario extends Fragment implements TimePickerDialog.OnTimeSetListener{

    HttpManager manager;
    Button reghor, acthor, h_ini, h_fin;
    EditText codigo;
    Boolean bandera = false;


    public RegAct_Horario(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.regact_horario, container, false);

        reghor = (Button)rootview.findViewById(R.id.reg_hor);
        acthor = (Button)rootview.findViewById(R.id.act_hor);
        codigo = (EditText)rootview.findViewById(R.id.cod_reg_hor);
        h_ini = (Button)rootview.findViewById(R.id.hini_reg_hor);
        h_fin = (Button)rootview.findViewById(R.id.hfin_reg_hor);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm", java.util.Locale.getDefault());

        h_ini.setText("00:00");
        h_ini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bandera = true;
                cambiarHora();
            }
        });

        h_fin.setText(sdf1.format(c.getTime()));
        h_fin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarHora();
            }
        });

        reghor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrar reg = new Registrar();
                reg.execute(h_ini.getText().toString(), h_fin.getText().toString());

            }
        });

        acthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actualizar act = new Actualizar();
                act.execute(codigo.getText().toString(), h_ini.getText().toString(), h_fin.getText().toString());
            }
        });

        return rootview;
    }

    private class Registrar extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {
                obj.put("hora_inicio", params[0]);
                obj.put("hora_fin", params[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/create1","POST",obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("true")){
                Toast.makeText(RegAct_Horario.this.getActivity(), "Horario Registrado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class Actualizar extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {
                obj.put("codigo_horario", params[0]);
                obj.put("hora_inicio", params[1]);
                obj.put("hora_fin", params[2]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/edit1","PUT",obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("true")){
                Toast.makeText(RegAct_Horario.this.getActivity(), "Horario Actualizado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void cambiarHora() {
        Dialog_Hora dialogoHora = new Dialog_Hora();
        dialogoHora.setOnTimeSetListener(this);
        dialogoHora.show(getActivity().getSupportFragmentManager(), "selectorHora");
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay,
                          int minute) {
        Calendar calendario = Calendar.getInstance();
        calendario.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendario.set(Calendar.MINUTE, minute);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",
                java.util.Locale.getDefault());
        if(bandera){
            h_ini.setText(sdf.format
                    (calendario.getTime()));
        }else{
            h_fin.setText(sdf.format
                    (calendario.getTime()));
        }
    }
}

