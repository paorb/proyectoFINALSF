package ejemplos.navigation2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import ejemplos.ws.HttpManager;

public class RegAct_Cliente extends Fragment {

    HttpManager manager;
    Button regcli, actcli;
    EditText codigo, nombre, apellido, celular, direccion, email;


    public RegAct_Cliente(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.regact_cliente, container, false);

        regcli = (Button)rootview.findViewById(R.id.reg_cli);
        actcli = (Button)rootview.findViewById(R.id.act_cli);
        codigo = (EditText)rootview.findViewById(R.id.cod_reg_cli);
        nombre = (EditText)rootview.findViewById(R.id.nom_reg_cli);
        apellido = (EditText)rootview.findViewById(R.id.ape_reg_cli);
        celular = (EditText)rootview.findViewById(R.id.cel_reg_cli);
        direccion= (EditText)rootview.findViewById(R.id.dir_reg_cli);
        email = (EditText)rootview.findViewById(R.id.ema_reg_cli);

        regcli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrar reg = new Registrar();
                reg.execute(nombre.getText().toString(), apellido.getText().toString(), celular.getText().toString(),
                        direccion.getText().toString(), email.getText().toString());

            }
        });

        actcli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actualizar act = new Actualizar();
                act.execute(codigo.getText().toString(), nombre.getText().toString(), apellido.getText().toString(),
                        celular.getText().toString(), direccion.getText().toString(), email.getText().toString());
            }
        });

        return rootview;
    }

    private class Registrar extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {

                obj.put("nombre_cliente", params[0]);
                obj.put("apellidos_cliente", params[1]);
                obj.put("celular", params[2]);
                obj.put("direccion", params[3]);
                obj.put("email", params[4]);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/create4","POST",obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("true")){
                Toast.makeText(RegAct_Cliente.this.getActivity(), "Cliente Registrado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class Actualizar extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            manager = new HttpManager();

            JSONObject obj = new JSONObject();
            try {

                obj.put("codigo_nombre", params[0]);
                obj.put("nombre_cliente", params[1]);
                obj.put("apellidos_cliente", params[2]);
                obj.put("celular", params[3]);
                obj.put("direccion", params[4]);
                obj.put("email", params[5]);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return manager.postput("http://192.168.1.5:1521/Service1.svc/edit4","PUT",obj.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("true")){
                Toast.makeText(RegAct_Cliente.this.getActivity(), "Cliente Actualizado", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

