package ejemplos.entidades;

public class Cliente {

    private int codigo;
    private String nombre;
    private String apellido;
    private String celular;
    private String direccion;
    private String email;

    public void setCodigo(int codigo){
        this.codigo = codigo;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setApellido(String apellido){
        this.apellido = apellido;
    }
    public void setCelular(String celular){
        this.celular = celular;
    }
    public void setDireccion(String direccion){
        this.direccion = direccion;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public int getCodigo(){
        return codigo;
    }
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public String getCelular(){
        return celular;
    }
    public String getDireccion(){
        return direccion;
    }
    public String getEmail(){
        return email;
    }
}
