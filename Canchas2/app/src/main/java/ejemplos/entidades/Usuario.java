package ejemplos.entidades;

public class Usuario {
    private int codigo;
    private String usuario;
    private String contras;

    public void setCodigo(int codigo){
        this.codigo = codigo;
    }

    public int getCodigo(){
        return codigo;
    }

    public void setUsuario(String usuario){
        this.usuario = usuario;
    }

    public String getUsuario(){
        return usuario;
    }
    public String getContras(){
        return contras;
    }
}
