package ejemplos.entidades;

public class Reserva {

    private int codigo;
    private String f_cre;
    private String f_res;
    private String valor;
    private int cancha;
    private int cliente;
    private int horario;

    public void setCodigo(int codigo){
        this.codigo = codigo;
    }
    public void setF_cre(String f_cre){
        this.f_cre = f_cre;
    }
    public void setF_res(String f_res){
        this.f_res = f_res;
    }
    public void setValor(String valor){
        this.valor = valor;
    }
    public void setCancha(int cancha){
        this.cancha = cancha;
    }
    public void setCliente(int cliente){
        this.cliente = cliente;
    }
    public void setHorario(int horario){this.horario = horario; }
    public int getCodigo(){
        return codigo;
    }
    public String getF_cre(){
        return f_cre;
    }
    public String getF_res(){
        return f_res;
    }
    public String getValor(){
        return valor;
    }
    public int getCancha(){
        return cancha;
    }
    public int getCliente(){
        return cliente;
    }
    public int getHorario(){
        return horario;
    }

}
