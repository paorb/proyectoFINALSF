package ejemplos.entidades;

public class Cancha {
    private int codigo;
    private String nombre;
    private String carac;

    public void setCodigo(int codigo){
        this.codigo = codigo;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setCarac(String carac){
        this.carac = carac;
    }
    public int getCodigo(){
        return codigo;
    }
    public String getNombre(){
        return nombre;
    }
    public String getCarac(){
        return carac;
    }
}
