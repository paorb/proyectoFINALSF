package ejemplos.entidades;

public class Horario {
    private int codigo;
    private String h_ini;
    private String h_fin;

    public void setCodigo(int codigo){
        this.codigo = codigo;
    }
    public void setH_ini(String h_ini){
        this.h_ini = h_ini;
    }
    public void setH_fin(String h_fin){
        this.h_fin = h_fin;
    }
    public int getCodigo(){
        return codigo;
    }
    public String getH_ini(){
        return h_ini;
    }
    public String getH_fin(){
        return h_fin;
    }
}
