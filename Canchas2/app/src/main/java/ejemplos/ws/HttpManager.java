package ejemplos.ws;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpManager {

    public String get(String uri) {
        BufferedReader buffer = null;
        URL url;

        try {
            url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            StringBuilder fullLines = new StringBuilder();
            buffer = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;

            while ((line = buffer.readLine()) != null) {
                fullLines.append(line);
            }

            return fullLines.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (buffer != null) {
                try {
                    buffer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String postput(String uri, String method, String write){

        URL url;
        BufferedReader br = null;

        try {
            url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod(method);


            DataOutputStream wr = new DataOutputStream(con.getOutputStream ());
            wr.writeBytes (write);
            wr.flush();
            wr.close();

            StringBuilder sb = new StringBuilder();
            br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;

            while ((line = br.readLine()) != null)
                sb.append(line);

            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void delete(String uri){

        URL url;

        try {

            url = new URL(uri);
                     HttpURLConnection con = (HttpURLConnection)url.openConnection();

            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestMethod("DELETE");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


